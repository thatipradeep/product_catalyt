//importing external dependencies
import express from "express";
import bodyParser from "body-parser";
import mongoose from "mongoose";

//importing crud handlers
import { post_handler } from "./crud_handlers/createdata/register_data.js";
import { get_handlers } from "./crud_handlers/getdata/getData.js";
// import { put_handlers } from "./crud_handlers/updatedata/updateData.js";
import { delete_handler } from "./crud_handlers/deletedata/deleteData.js";
import { getall_handlers } from "./crud_handlers/getdata/getall.js";
import { put_handlers } from "./crud_handlers/updatedata/updateData.js";
// import { post_validator } from "./validators/post_validator.js";
// import { get_validator } from "./validators/get_validator.js";
// import { delete_validator } from "./validators/delete_validator.js";
// import { put_validator } from "./validators/put_validator.js";


import cors from "cors";

const app = express();
app.use(
  cors({
    origin: "*",
  })
);
//Returns middleware that only parses json
app.use(bodyParser.json());
//Returns middleware that only parses urlencoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

/********************Post Call************************/
app.post("/createdata", post_handler);

/**********************GET CALL************************/
app.get("/getdata", get_handlers); 

app.post("/getdata", get_handlers);

app.get("/getalldata",getall_handlers);
/**********************PUT CALL*********************/
// app.put("/updatedata", put_handlers);
app.put("/updatedata", put_handlers)

/*********************DELETE CALL*********************/
app.delete("/deletedata", delete_handler);

//server listeining to the port
app.listen(5000, () => {
  console.log("server started and listnening to the port 5000");
});
