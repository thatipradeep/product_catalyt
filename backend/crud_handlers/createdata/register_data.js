import {ProductData} from '../../utils/models.js'
/**
 * @param {*} req product_name, product_description, product_price, discount, merchant_name,quantity
 * @param {*} res sends status codes and status messages as response
 * @param {*} next  it is an express middleware
 * if created scussesfull sending a message that product data registration successful as a response
 *status codes-201(for sucessful)  
 */

function post_handler(request, response) {
  const { product_name, product_description, product_price, discount, merchant_name,quantity } =
    request.body;
  const product_details = {
    product_name,
    product_description,
    product_price,
    discount,
    merchant_name,
    quantity,
  };

  const product_data = new ProductData(product_details);

  product_data.save()
    .then(() => {
      console.log("data saved");
      response.send({ msg: "product register successfully",status_code:200 });
    })
    .catch((err) => {
      console.log(err);
    });
}

export { post_handler };
