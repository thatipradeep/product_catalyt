//internal dependency
import { ProductData } from "../../utils/models.js";
/**
 * @param {*} req new_product_price,product_name,
 * @param {*} res sends status codes and status messages as response
 * if updated scussesfull sending a message that updated product_price successful as a response
 */
function put_handlers(request, response) {
  const { product_name, new_product_price } = request.body;

  ProductData.findOne({ product_name }, (err, dataObj) => {
    if (err) {
      response.status(500).send("db error", err);
    } else {
      if (dataObj === null) {
        response.status(400).send({msg:"Product name not exist"});
      } else {
        dataObj.updateOne(
          { $set: { product_price: new_product_price } },
          (err) => {
            if (err) {
              response.send({msg:"unable to update product_price"});
            } else {
              response.send({
                msg: "product_price updated successfully",
                status_code: 200,
              });
            }
          }
        );
      }
    }
  });
}

export { put_handlers };
