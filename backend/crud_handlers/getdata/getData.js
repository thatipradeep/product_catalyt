import { ProductData } from "../../utils/models.js";
/**
 * @param {*} req enter the product_name for get the user details
 * @param {*} res then all the details got available
 * if retrived scussesfully sending a message that data retrived successful as a response
 */

function get_handlers(request, response) {
  const { product_name } = request.body;
  ProductData.findOne(
    { product_name },
    "product_name product_description discount product_price merchant_name quantity -_id",
    (err, dataObj) => {
      if (err) {
        response.status(500).send("databse err", err);
      } else {
        if (dataObj === null) {
          response.send({ msg: "Product name doesn't exist" });
        } else {
          response.status(200).send({ obj: dataObj, statuscode: 200 });
        }
      }
    }
  );
}

export { get_handlers };
