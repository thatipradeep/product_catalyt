import { ProductData } from "../../utils/models.js";
/**
 * @param {*} req enter the product_name for get the user details
 * @param {*} res then all the details got available
 * if retrived scussesfully sending a message that data retrived successful as a response
 */

function getall_handlers(request, response) {
  ProductData.find({ },
    "product_name product_description discount product_price merchant_name quantity -_id",
    (err, data) => {
      if (err) {
        response.status(500).send("databse err", err);
      } else {
          response.status(200).json(data);
        }
      }
  );
}

export { getall_handlers };
