import { ProductData } from "../../utils/models.js";
/**
 * @param {*} req Product_name whcih the handler has to be deleted
 * @param {*} res sends status codes and status messages as response
 * if deleted scussesfull sending a message that user data Deleted successfully as a response
 * 
 */
function delete_handler(request, response){
    const {product_name} = request.body
    ProductData.findOne({product_name},(err, dataObj)=>{
        if (err){
            response.status(500).send("Database error")
        }else{
            if(dataObj == null){
                response.status(401).send({msg:"Product name doesn't exist"})
            } else {
                dataObj.delete((err)=>{
                    if (err){
                        response.send("unable to delete product data")
                    }else{
                        response.send({msg:"Deleted product details successfully", status_code:200})
                    }
                })
            }
        }
    })
}
export {delete_handler}