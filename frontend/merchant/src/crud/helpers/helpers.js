/**
 * Helper function to validate product_name string
 * checks if name is string, else returns "name should be of string type"
 * checks if name is too short, else returns "name should be at least 4 characters long and max of 25"
 * checks if name is of the correct format, else returns "name should not have at least 1 special character and at least 1 digit"
 * @param {} product_name
 * @returns
 */
 function product_name_helper(product_name) {
  const returnObject = {};
  // is it a string
  if (product_name === undefined) {
    returnObject.product_name = "Please give Your Product Name";
    return returnObject;
  }
  if (typeof product_name !== "string") {
    returnObject.product_name = "Product Name should be of string type";
    return returnObject;
  }
  if (product_name.length === 0) {
    returnObject.product_name = "Product Name field is mandatory to fill";
    return returnObject;
  }
  return returnObject;
}


/**
 * Helper function to validate merchantname string
 * checks if merchantname is string, else returns "merchantname should be of string type"
 * checks if merchantname is too short, else returns "merchantname should be at least 4 characters long and max of 25"
 * checks if merchantname is of the correct format, else returns "merchantname should not have at least 1 special character and at least 1 digit"
 * @param {} merchantname
 * @returns
 */
 function product_merchant_name_helper(merchant_name) {
  const returnObject = {};
  // is it a string
  if (merchant_name === undefined) {
    returnObject.merchant_name = "Please give Your Merchant Name";
    return returnObject;
  }
  if (typeof merchant_name !== "string") {
    returnObject.merchant_name = "Merchant Name should be of string type";
    return returnObject;
  }
  if (merchant_name.length === 0) {
    returnObject.merchant_name = "Merchant Name field is mandatory to fill";
    return returnObject;
  }
  return returnObject;
}


/**
 * Helper function to validate price
 * checks if price is number, else returns "price should be of number type"
 * checks if price is too short, else returns "price should be at least 3 characters long and max of 10"
 * checks if price is of the correct format, else returns "priceshould be of the correct format" 406
 * @param {} price
 * @returns
 */
 function product_price_helper(product_price) {
  const returnObject = {};
  if (product_price === undefined) {
    returnObject.product_price = "Product Price field is mandatory to fill";
    return returnObject;
  }
  if(product_price.toString().length === 0){
    returnObject.product_price = "Product Price field is mandatory to fill";
    return returnObject;
  }
  if (isNaN(product_price)) {
    returnObject.product_price = "Product Price should always be a number type";
    return returnObject;
  }
 
  return returnObject;
}


/**
 * Helper function to validate phone number
 * checks if new_product_price is number, else returns "new_product_price should be of number type"
 * checks if new_product_price is too short, else returns "new_product_price should be at least 3 characters long and max of 10"
 * checks if new_product_price is of the correct format, else returns "new_product_price should be of the correct format" 406
 * @param {} new_product_price
 * @returns
 */ 
 function new_product_price_helper(new_product_price) {
  const returnObject = {};
  if (new_product_price === undefined) {
    returnObject.new_product_price = "New Price field is mandatory to fill";
    return returnObject;
  }
  if(new_product_price.toString().length === 0){
    returnObject.new_product_price = "New Price field is mandatory to fill";
    return returnObject;
  }
  if (isNaN(new_product_price)) {
    returnObject.new_product_price = "New Price should always be a number type";
    return returnObject;
  }
 
  return returnObject;
}

/**
 * Helper function to validate discount
 * checks if discount is number, else returns "discount should be of number type"
 * checks if discount is too short, else returns "discount should be at least 3 characters long and max of 10"
 * checks if discountis of the correct format, else returns "discount should be of the correct format" 406
 * @param {} discount
 * @returns
 */
 function product_discount_helper(discount) {
  const returnObject = {};
  if (discount === undefined) {
    returnObject.discount = "Discount field is mandatory to fill";
    return returnObject;
  }
  if(discount.toString().length === 0){
    returnObject.discount = "Discount field is mandatory to fill";
    return returnObject;
  }
  if (isNaN(discount)) {
    returnObject.discount = "Discount should always be a number type";
    return returnObject;
  }
  

  return returnObject;
}

/**
 * Helper function to validate quantity
 * checks if quantity is number, else returns "quantity should be of number type"
 * checks if quantity is too short, else returns "quantity should be at least 1 characters long and max of 10"
 * checks if quantity is of the correct format, else returns "quantity should be of the correct format" 406
 * @param {} Quantity
 * @returns
 */
 function product_quantity_helper(quantity) {
  const returnObject = {};
  if (quantity === undefined) {
    returnObject.quantity = "Quantity field is mandatory to fill";
    return returnObject;
  }
  if(quantity.toString().length === 0){
    returnObject.quantity = "Quantity field is mandatory to fill";
    return returnObject;
  }
  if (isNaN(quantity)) {
    returnObject.quantity = "Quantity should always be a number type";
    return returnObject;
  }
 

  return returnObject;
}

/**
  
   * This is a helper function to check and validate the description
  
   * @param {} description this is the parameter recieved to validate
  
   * @returns An object containing error messages.
  
   */

function product_description_helper(product_description) {
  const returnObject = {};

  // is it a string

  if (product_description === undefined) {
    returnObject.product_description = "Please give Your product_description";

    return returnObject;
  }
  if(product_description.toString().length === 0){
    returnObject.product_description = "Poduct Description field is mandatory to fill";
    return returnObject;
  }
  if (product_description === undefined){
    returnObject.product_description = "mandotary";
    return returnObject;
}

  if (typeof product_description != "string") {
    returnObject.product_description =
      "product_description should be of string type";

    return returnObject;
  }
}

function returnObjectKeys(obj) {
  let returnArray = [];

  for (let key in obj) {
    returnArray.push(key);
  }
  return returnArray;
}

export {
  product_description_helper,
  product_merchant_name_helper,
  product_name_helper,
  product_quantity_helper,
  product_discount_helper,
  product_price_helper,
  new_product_price_helper,
  returnObjectKeys,
};
