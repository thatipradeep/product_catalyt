import React from "react";
import "./crud.css";
import {
  product_description_helper,
  product_merchant_name_helper,
  product_name_helper,
  product_quantity_helper,
  product_discount_helper,
  product_price_helper,
  returnObjectKeys,
} from "./helpers/helpers.js";

class RegisterCrud extends React.Component {
  state = {
    product_name: "",
    product_name_err: "",
    product_description: "",
    product_description_err: "",
    product_price: "",
    product_price_err: "",
    discount: "",
    discount_err: "",
    merchant_name: "",
    merchant_name_err: "",
    quantity: "",
    quantity_err: "",
  };

  RegisterSuccess = () => {
    const { history } = this.props;
    history.push("/createcode");
  };

  apiCallFail = (data) => {
    this.setState({ error_msg: data.err_msg });
  };

  registerApiCall = async (event) => {
    event.preventDefault();
    console.log(this.state);

    const {
      product_name,
      product_description,
      product_price,
      discount,
      merchant_name,
      quantity,
    } = this.state;
    const url = "http://localhost:5000/createdata";
    const userDetails = {
      product_name,
      product_description,
      product_price: parseInt(product_price),
      discount: parseInt(discount),
      merchant_name,
      quantity: parseInt(quantity),
    };
    const option = {
      method: "POST",
      body: JSON.stringify(userDetails),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const response = await fetch(url, option);
    const data = await response.json();
    console.log(data);
    if (data.status_code === 200) {
      this.RegisterSuccess();
    } else {
      this.apiCallFail(data);
    }
  };

  changeproduct_name = (event) => {
    this.setState({ product_name: event.target.value, product_name_err: "" });
  };
  validateproduct_name = () => {
    const product_name = this.state.product_name;
    const product_name_errors = product_name_helper(product_name);
    const is_product_name_validated =
      returnObjectKeys(product_name_errors).length === 0;
    console.log(product_name_errors);
    console.log(is_product_name_validated);
    if (!is_product_name_validated) {
      this.setState({ product_name_err: product_name_errors.product_name });
    }
  };

  changeproduct_description = (event) => {
    this.setState({
      product_description: event.target.value,
      product_description_err: "",
    });
  };
  validateproduct_description = () => {
    const product_description = this.state.product_description;
    const product_description_errors =
      product_description_helper(product_description);
    const is_product_description_validated =
      returnObjectKeys(product_description_errors).length === 0;
    console.log(product_description_errors);
    console.log(is_product_description_validated);
    if (!is_product_description_validated) {
      // firstname validation failed
      this.setState({
        product_description_err: product_description_errors.product_description,
      });
    }
  };

  changeproduct_price = (event) => {
    this.setState({ product_price: event.target.value, product_price_err: "" });
  };
  validateproduct_price = () => {
    const product_price = this.state.product_price;
    const product_price_errors = product_price_helper(product_price);
    const is_product_price_validated =
      returnObjectKeys(product_price_errors).length === 0;
    console.log(product_price_errors);
    console.log(is_product_price_validated);
    if (!is_product_price_validated) {
      this.setState({ product_price_err: product_price_errors.product_price });
    }
  };

  changediscount = (event) => {
    this.setState({ discount: event.target.value, discount_err: "" });
  };
  validatediscount = () => {
    const discount = this.state.discount;
    const discount_errors = product_discount_helper(discount);
    const is_discount_validated =
      returnObjectKeys(discount_errors).length === 0;
    console.log(discount_errors);
    console.log(is_discount_validated);
    if (!is_discount_validated) {
      this.setState({ discount_err: discount_errors.discount });
    }
  };

  changemerchant_name = (event) => {
    this.setState({ merchant_name: event.target.value, merchant_name_err: "" });
  };
  validatemerchant_name = () => {
    const merchant_name = this.state.merchant_name;
    const merchant_name_errors = product_merchant_name_helper(merchant_name);
    const is_merchant_name_validated =
      returnObjectKeys(merchant_name_errors).length === 0;
    console.log(merchant_name_errors);
    console.log(is_merchant_name_validated);
    if (!is_merchant_name_validated) {
      this.setState({ merchant_name_err: merchant_name_errors.merchant_name });
    }
  };

  changequantity = (event) => {
    this.setState({ quantity: event.target.value, quantity_err: "" });
  };
  validatequantity = () => {
    const quantity = this.state.quantity;
    const quantity_errors = product_quantity_helper(quantity);
    const is_quantity_validated =
      returnObjectKeys(quantity_errors).length === 0;
    console.log(quantity_errors);
    console.log(is_quantity_validated);
    if (!is_quantity_validated) {
      this.setState({ quantity_err: quantity_errors.quantity });
    }
  };

  render() {
    return (
      <>
        <div className="bablureddyt">
          <div className="main">
            <p className="sign" align="center">
              Enter Product Details
            </p>
            <form className="form1" onSubmit={this.registerApiCall}>
              <input
                className="un"
                type="text"
                align="center"
                placeholder="ProductName"
                required
                onChange={this.changeproduct_name}
                onBlur={this.validateproduct_name}
              />
              <p align="center" style={{ color: "white", fontSize: "15px" }}>
                {this.state.product_name_err}
              </p>
              <input
                className="un"
                type="text"
                align="center"
                placeholder="ProductDescription"
                required
                onChange={this.changeproduct_description}
                onBlur={this.validateproduct_description}
              />
              <p align="center" style={{ color: "white", fontSize: "15px" }}>
                {this.state.product_description_err}
              </p>
              <input
                className="un"
                type="text"
                align="center"
                placeholder="ProductPrice"
                required
                onChange={this.changeproduct_price}
                onBlur={this.validateproduct_price}
              />
              <p align="center" style={{ color: "white", fontSize: "15px" }}>
                {this.state.product_price_err}
              </p>
              <input
                className="un"
                type="text"
                align="center"
                placeholder="Discount"
                required
                onChange={this.changediscount}
                onBlur={this.validatediscount}
              />
              <p align="center" style={{ color: "white", fontSize: "15px" }}>
                {this.state.discount_err}
              </p>
              <input
                className="un"
                type="text"
                align="center"
                placeholder="MerchantName"
                required
                onChange={this.changemerchant_name}
                onBlur={this.validatemerchant_name}
              />
              <p align="center" style={{ color: "white", fontSize: "15px" }}>
                {this.state.merchant_name_err}
              </p>
              <input
                className="un"
                type="text"
                align="center"
                placeholder="Quantity"
                required
                onChange={this.changequantity}
                onBlur={this.validatequantity}
              />
              <p align="center" style={{ color: "white", fontSize: "15px" }}>
                {this.state.quantity_err}
              </p>
              <button className="submit" align="center">
                Register
              </button>
              <p className="forgot" align="center"></p>
              <a href="#"></a>
            </form>
          </div>
          <a href="#"></a>
        </div>
      </>
    );
  }
}

export default RegisterCrud;
