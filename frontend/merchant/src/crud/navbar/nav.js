import React from "react";
import {Navbar, Container, NavDropdown, Nav} from 'react-bootstrap'


class Navigate extends React.Component {
  render() {
    return (
      <>
        
     <Navbar bg="warning" variant="Danger" expand="lg">
  <Container>
  <NavDropdown title="Actions" id="basic-nav-dropdown">
          <NavDropdown.Item href="/createdata">Create</NavDropdown.Item>
          <NavDropdown.Item href="/getdata">Get Single Data</NavDropdown.Item>
          <NavDropdown.Item href="/getalldata">Get all Product Data</NavDropdown.Item>
          <NavDropdown.Item href="/updatedata">Update</NavDropdown.Item>
          <NavDropdown.Item href="/deletedata">Delete</NavDropdown.Item>
          <NavDropdown.Divider />
          <NavDropdown.Item href="/">Home</NavDropdown.Item>
        </NavDropdown>
    <Navbar.Brand href="#home"></Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="me-auto">
        <Nav.Link href="/">Home</Nav.Link>
        <Nav.Link href="/createdata">Create</Nav.Link>
        <Nav.Link href="/getdata">Get Single Data</Nav.Link>
        <Nav.Link href="/getalldata">Get all Product Data</Nav.Link>
        <Nav.Link href="/updatedata">Update</Nav.Link>
        <Nav.Link href="/deletedata">Delete</Nav.Link>
     
      </Nav>
    </Navbar.Collapse>
  </Container>
</Navbar>
</>
      
    );
  }
}
export default Navigate ;
