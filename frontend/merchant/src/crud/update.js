import React from "react";
// import { Navbar } from "react-bootstrap";
import "./crud.css";

//import { Alert } from "react-alert";
// import {Link} from 'react-router-dom'
// import {}
import {
  product_name_helper,
  new_product_price_helper,
  returnObjectKeys,
} from "./helpers/helpers.js";

class UpdateCrud extends React.Component {
  state = {
    product_name: "",
    product_name_err: "",
    new_product_price: "",
    new_product_price_err: "",
    product_name_db_err: "",
  };

  UpdateSuccess = () => {
    const { history } = this.props;
    history.push("/updatecode");
  };

  apiCallFail = (data) => {
    this.setState({product_name_db_err: data.msg });
  };

  updateApiCall = async (event) => {
    event.preventDefault();
    console.log(this.state);

    const { product_name, new_product_price } = this.state;
    const url = "http://localhost:5000/updatedata";
    const userDetails = {
      product_name,
      new_product_price,
    };
    const option = {
      method: "PUT",
      body: JSON.stringify(userDetails),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const response = await fetch(url, option);
    const data = await response.json();
    console.log(data);
    if (data.status_code === 200) {
      console.log(data);
      this.UpdateSuccess();
    } else {
      this.apiCallFail(data);
    }
  };

  changeproduct_name = (event) => {
    this.setState({ product_name: event.target.value, product_name_err: "" });
  };
  validateproduct_name = () => {
    const product_name = this.state.product_name;
    const product_name_errors = product_name_helper(product_name);
    const is_product_name_validated =
      returnObjectKeys(product_name_errors).length === 0;
    console.log(product_name_errors);
    console.log(is_product_name_validated);
    if (!is_product_name_validated) {
     
      this.setState({ product_name_err: product_name_errors.product_name });
    }
  };

  changenew_product_price = (event) => {
    this.setState({
      new_product_price: event.target.value,
      new_product_price_err: "",
    });
  };
  validatenew_product_price = () => {
    const new_product_price = this.state.new_product_price;
    const new_product_price_errors =
      new_product_price_helper(new_product_price);
    const is_new_product_price_validated =
      returnObjectKeys(new_product_price_errors).length === 0;
    console.log(new_product_price_errors);
    console.log(is_new_product_price_validated);
    if (!is_new_product_price_validated) {
    
      this.setState({
        new_product_price_err: new_product_price_errors.new_product_price,
      });
    }
  };

  render() {
    return (
      <>
        <div className="bablureddyt">
          <div className="main">
            <p className="sign" align="center">
              Update Product Details
            </p>
            <form className="form1" onSubmit={this.updateApiCall}>
              <input
                className="un "
                type="text"
                align="center"
                placeholder="ProductName"
                required
                onChange={this.changeproduct_name}
                onBlur={this.validateproduct_name}
              />
              <p align="center" style={{ color: "red" }}>
                {this.state.product_name_err}
              </p>
              <p align="center" style={{ color: "white", fontSize: "15px" }}>
                {this.state.product_name_db_err}
              </p>
              <input
                className="un "
                type="text"
                align="center"
                placeholder="NewProductPrice"
                required
                onChange={this.changenew_product_price}
                onBlur={this.validatenew_product_price}
              />
              <p align="center" style={{ color: "white", fontSize: "15px" }}>
                {this.state.new_product_price_err}
              </p>

              <button className="submit" align="center">
                Update
              </button>
              <p className="forgot" align="center"></p>
              <a href="/#"></a>
            </form>
          </div>
          <a href="/#"></a>
        </div>
      </>
    );
  }
}

export default UpdateCrud;
