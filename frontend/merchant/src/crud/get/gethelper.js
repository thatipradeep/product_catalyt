import {
  Table,
  TableHead,
  TableCell,
  TableRow,
  TableBody,
  Button,
  makeStyles,
} from "@material-ui/core";
import { Link } from "react-router-dom";

const useStyles = makeStyles({
  table: {
    width: "100%",

    marginTop: "1px",
  },

  thead: {
    "& > *": {
      fontSize: 18,

      background: "#eb7907",

      color: "#FFFFFF",
    },
  },

  row: {
    "& > *": {
      fontSize: 15,
      color: "white",
    },
  },

  button1: {
    backgroundColor: "cyan",
  },

  button2: {
    backgroundColor: "red",
    color:"white",
  },
  button3: {
    backgroundColor: "green",
    color:"white",
  },

  background: {
    overflowX: "auto",
  },
});

const AllProductList = () => {
  const classes = useStyles();
  const data = localStorage.getItem("productdata")
  const productdata = JSON.parse(data)
  console.log(productdata)
  return (
    <>
    <div className="bablureddyt">
    {/* <h1>Product Catalog Details</h1> */}
    <Table className={classes.table}>
        <TableHead>
        <TableRow className={classes.thead}>
          <TableCell>Name</TableCell>
          <TableCell>Description</TableCell>
          <TableCell>Price</TableCell>
          <TableCell>Discount</TableCell>
          <TableCell>MerchantName</TableCell>
          <TableCell>Quantity</TableCell>
          <TableCell>UPDTAE</TableCell>
          <TableCell>DELETE</TableCell>
          <TableCell>CREATE</TableCell>
          <TableCell></TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow className={classes.row}>
          <TableCell>{productdata.obj.product_name}</TableCell>
          <TableCell>{productdata.obj.product_description}</TableCell>
          <TableCell>{productdata.obj.product_price}</TableCell>
          <TableCell>{productdata.obj.discount}</TableCell>
          <TableCell>{productdata.obj.merchant_name}</TableCell>
          <TableCell>{productdata.obj.quantity}</TableCell>
          <TableCell>
                  <Button
                    className={classes.button1}
                    variant="primary"
                    style={{ marginRight: 10 }}
                    component={Link}
                    to={`/updatedata`}
                  >
                    UPDATE
                  </Button>{" "}
                </TableCell>
                <TableCell>
                  <Button
                    className={classes.button2}
                    variant="primary"
                    component={Link}
                    to={`/deletedata`}
                  >
                    Delete
                  </Button>{" "}
                </TableCell>
                <TableCell>
                  <Button
                    className={classes.button3}
                    variant="primary"
                    component={Link}
                    to={`/createdata`}
                  >
                    Create
                  </Button>{" "}
                </TableCell>
                </TableRow>   
      </TableBody>
    </Table>
    </div>
    </>
  );
};

export default  AllProductList;
