import React, { Component } from "react";
import { product_name_helper, returnObjectKeys } from "../helpers/helpers.js";

class GetProductData extends Component {
  state = {
    product_name: " ",
    product_name_err: "",
    product_name_db_err: "",
  };

  getSuccess = () => {
    const { history } = this.props;
    history.push("/productdetails");
  };

  apiCallFail = (data) => {
    this.setState({ product_name_db_err: data.msg });
  };

  getApiCall = async (event) => {
    event.preventDefault();
    console.log(this.state);
    const { product_name } = this.state;
    const url = `http://localhost:5000/getdata`;
    const userDetails = {
      product_name,
    };
    const option = {
      method: "POST",
      body: JSON.stringify(userDetails),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const response = await fetch(url, option);
    const data = await response.json();
    if (data.statuscode === 200) {
      console.log(data);
      localStorage.setItem("productdata", JSON.stringify(data));
      this.getSuccess();
    } else {
      this.apiCallFail(data);
    }
  };
  changeproduct_name = (event) => {
    this.setState({ product_name: event.target.value, product_name_err: "" });
  };
  validateproduct_name = () => {
    const product_name = this.state.product_name;
    const product_name_errors = product_name_helper(product_name);
    const is_product_name_validated =
      returnObjectKeys(product_name_errors).length === 0;
    console.log(product_name_errors);
    console.log(is_product_name_validated);
    if (!is_product_name_validated) {
      this.setState({ product_name_err: product_name_errors.product_name });
    }
  };
  render() {
    return (
      <>
        <div className="bablureddyt">
          <div className="main">
            <p className="sign" align="center">
              Get Product Details
            </p>
            <form className="form1" onSubmit={this.getApiCall}>
              <input
                className="un "
                type="text"
                align="center"
                placeholder="ProductName"
                required
                onChange={this.changeproduct_name}
                onBlur={this.validateproduct_name}
              />
              <p align="center" style={{ color: "red" }}>
                {this.state.product_name_err}
              </p>
              <p align="center" style={{ color: "white", fontSize: "15px" }}>
                {this.state.product_name_db_err}
              </p>
              <button className="submit" align="center">
                Get
              </button>
              <p className="forgot" align="center"></p>
              <a href="#"></a>
            </form>
          </div>
          <a href="#"></a>
        </div>
      </>
    );
  }
}
export { GetProductData };
