import { useState, useEffect } from "react";
// import './getall.css'
import { Link } from "react-router-dom";
// import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import {
  Table,
  TableHead,
  TableCell,
  TableRow,
  TableBody,
  TableContainer,
  Button,
  makeStyles,
} from "@material-ui/core";

const useStyles = makeStyles({
  table: {
    width: "100%",

    marginTop: "1x",
  },

  thead: {
    "& > *": {
      fontSize: 18,

      background: "#eb7907",

      color: "white",
    },
  },

  row: {
    "& > *": {
      fontSize: 15,
      color: "white",
    },
  },

  button1: {
    backgroundColor: "cyan",
  },

  button2: {
    backgroundColor: "red",
    color: "white",
  },
  button3: {
    backgroundColor: "green",
    color: "white",
  },

  background: {
    overflowX: "auto",
  },
});
const GetAllProductData = () => {
  const [product_data, setProduct] = useState([]);
  const classes = useStyles();

  useEffect(() => {
    async function getApicall() {
      const option = {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      };
      
      const response = await fetch("http://localhost:5000/getalldata", option);
      const data = await response.json();
      console.log(data);
      setProduct(data);
    }
    getApicall();
  }, []);
  

  return (
    <div className="bablureddyt">
      <div className={classes.background}>
        <TableContainer className={classes.root}>
          <Table className={classes.table} id="table-to-xls">
            <TableHead>
              <TableRow className={classes.thead}>
                <TableCell>Name</TableCell>
                <TableCell>Description</TableCell>
                <TableCell>Price</TableCell>
                <TableCell>Discount</TableCell>
                <TableCell>MerchantName</TableCell>
                <TableCell>Quantity</TableCell>
                <TableCell>EDIT</TableCell>
                <TableCell>DELETE</TableCell>
                <TableCell>CREATE</TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {product_data.map((productdata) => (
                <TableRow className={classes.row} key={productdata.id}>
                  <TableCell>{productdata.product_name}</TableCell>
                  <TableCell>{productdata.product_description}</TableCell>
                  <TableCell>{productdata.product_price}</TableCell>
                  <TableCell>{productdata.discount}</TableCell>
                  <TableCell>{productdata.merchant_name}</TableCell>
                  <TableCell>{productdata.quantity}</TableCell>
                  <TableCell>
                    <Button
                      className={classes.button1}
                      variant="primary"
                      style={{ marginRight: 10 }}
                      component={Link}
                      to={`/updatedata`}
                    >
                      UPDATE
                    </Button>{" "}
                  </TableCell>
                  <TableCell>
                    <Button
                      className={classes.button2}
                      variant="primary"
                      component={Link}
                      to={`/deletedata`}
                    >
                      Delete
                    </Button>{" "}
                  </TableCell>
                  <TableCell>
                    <Button
                      className={classes.button3}
                      variant="primary"
                      component={Link}
                      to={`/createdata`}
                    >
                      Create
                    </Button>{" "}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    </div>
  );
};
export { GetAllProductData };
