import React from "react";
import "./status.css";

class Createcode extends React.Component {
  render() {
    return (
      <div className="barre">
        <div className="ibomma">
          <i className="checkmark">✓</i>
          <h1>Created Successfully</h1>
        </div>
      </div>
    );
  }
}

export default Createcode;
