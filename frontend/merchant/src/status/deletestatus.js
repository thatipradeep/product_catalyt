import React from "react";
import "./status.css";

class Deletecode extends React.Component {
  render() {
    return (
      <div className="barre">
        <div className="ibomma">
          <i className="checkmark">✓</i>
          <h1>Deleted Successfully</h1>
        </div>
      </div>
    );
  }
}

export default Deletecode;
