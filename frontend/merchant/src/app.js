import { BrowserRouter, Route, Switch } from "react-router-dom";
import React from "react";
import RegisterCrud from "./crud/register.js";
import Home from "./home/home.js";
import UpdateCrud from "./crud/update.js";
import DeleteCrud from "./crud/delete.js";
import Navigate from "./crud/navbar/nav.js"
import AllProductList from "./crud/get/gethelper.js";
import { GetProductData } from "./crud/get/getdata.js";
import {GetAllProductData} from "./crud/getAll_Data/getall.js";
import Createcode from "./status/createstatus.js";
import Deletecode from "./status/deletestatus.js";
import Updatedcode from "./status/updatestatus.js";

function App() {
  return (
    <>
    <Navigate/>
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/createdata" exact component={RegisterCrud} />
        <Route path="/createcode" exact component={Createcode} />
        <Route path="/updatedata" exact component={UpdateCrud} />
        <Route path="/updatecode" exact component={Updatedcode} />
        <Route path="/deletedata" exact component={DeleteCrud} />
        <Route path="/deletecode" exact component={Deletecode} />
        <Route path="/productdetails" exact component={AllProductList} />
        <Route path="/getdata" exact component={GetProductData} />
        <Route path="/getalldata" exact component={GetAllProductData} />
       
      
      </Switch>
    </BrowserRouter>
    </>
  );
}

export default App;
