import React from "react";

import { Link } from "react-router-dom";

class Landing extends React.Component {
  render() {
    return (
      <>
      <div className="bablureddyt">
        <div className="main">
          <p className="sign" align="center">
            PRODUCT CATALOG CRUD OPERATIONS
          </p>
          <form className="form1" >
           <Link to="/createdata"> <h4><button className="submit" align="center">
              CREATE DATA
            </button></h4><br></br></Link>
            <Link to="/getdata">  <h4> <button className="submit" align="center">
              RETRIVE DATA
            </button></h4><br></br></Link>
            <Link to="/getalldata">  <h4> <button className="submit" align="center">
              RETRIVE ALL DATA
            </button></h4><br></br></Link>
            <Link to="/updatedata">  <h4> <button className="submit" align="center">
              UPDATE DATA
            </button></h4><br></br></Link>
            <Link to="/deletedata">  <h4> <button className="submit" align="center">
              DELETE DATA
            </button></h4><br></br></Link>
            <p className="forgot" align="center"></p>
            <a href="/#"></a>
          </form>
        </div>
        <a href="/#"></a>
        </div>
        
      </>
    );
  }
}

export default Landing;
